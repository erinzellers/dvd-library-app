﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDLibrary.Data;
using DVDLibrary.Models;

namespace DVDLibrary.BLL
{
    public class Manager
    {
        public Response<List<Movie>> GetAllMovies(string searchString)
        {
            MovieRepository repository = new MovieRepository();

            Response<List<Movie>> response = new Response<List<Movie>>();

            try
            {
                var movieList = repository.GetMovieList(searchString);
                if (movieList.Count != 0)
                {
                    response.Success = true;
                    response.Data = movieList;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Please add a movie because there are currently no movies.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Sorry, there was an error in retrieving the Movies.";

            }
            return response;
        }

        public Response<Movie> MovieAdd(Movie MovieToAdd)
        {
            MovieRepository repo = new MovieRepository();
            Response<Movie> response = new Response<Movie>();

            try
            {
                int newIDnumber = repo.AddMovie(MovieToAdd);
                //if (repo.GetMovieList().Any(c => c.MovieID == MovieToAdd.MovieID))
                if (newIDnumber != 0)
                {
                    response.Success = true;
                    response.Message = "You have added a Movie to the library.";
                    response.Data = MovieToAdd;
                }
                else
                {
                    response.Success = false;
                    response.Message = "The Movie could not be added.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Sorry, there was an error adding a movie.";
            }
            return response;
        }

        public Response<Movie> MovieDelete(int movieId)
        {
            MovieRepository repo = new MovieRepository();
            Response<Movie> response = new Response<Movie>();

            try
            {
                repo.RemoveMovie(movieId);

                Movie shouldNotBeFound = repo.GetMovieInfo(movieId);
                if (shouldNotBeFound.MovieID == 0)
                {
                    response.Success = true;
                    response.Message = "You have deleted a Movie.";
                    //response.Data = movieToDelete;
                }
                else
                {
                    response.Success = false;
                    response.Message = "The movie could not be deleted.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Sorry, there was an error deleting this movie.";
            }
            return response;
        }

        public Response<Movie> ViewSingleMovie(int movieID)
        {
            var response = new Response<Movie>();
            var movieRepo = new MovieRepository();

            try
            {
                var movieFound = movieRepo.GetMovieInfo(movieID);

                if (movieFound.MovieID == movieID)
                {
                    response.Success = true;
                    response.Data = movieFound;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Requested movie could not be found.";
                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = string.Format("Movie not found: {0}", ex.Message);
            }

            return response;
        }

        public Response<Movie> AddActorToMovie(Actor newActor, int movieID)
        {
            var repo = new MovieRepository();
            var response = new Response<Movie>();

            try
            {
                var newActorID = repo.AddActorToMovie(movieID, newActor);
                var updatedMovie = repo.GetMovieInfo(movieID);

                if (newActorID != 0)
                {
                    response.Success = true;
                    response.Data = updatedMovie;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Actor listing not added.";
                }
            }

            catch (Exception ex)
            {
                response.Success = false;
                response.Message = string.Format("Error: {0}. Actor could not be added.");
            }

            return response;
        }
    }
}
