﻿using DVDLibrary.Data.Config;
using DVDLibrary.Models ;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Data
{
    public class MovieRepository
    {
        private SqlConnection _connection = new SqlConnection(Settings.ConnectionString);

        public List<Movie> GetMovieList(string searchString = "")
        {
            var movieList = new List<Movie>();

            using (var cn = _connection)
            {
                var cmd = new SqlCommand();

                cmd.CommandText = "GetListByTitle";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;

                string sqlSearch = string.Format("%{0}%", searchString);
                cmd.Parameters.AddWithValue("@movietitle", sqlSearch);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        movieList.Add(MovieDataReader(dr));
                    }
                }
            }
            return movieList;
        }

        public Movie GetMovieInfo(int movieID)
        {
            Movie movie = new Movie();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();

                cmd.CommandText = "GetMovieDetail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@movieid", movieID);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        movie = MovieDataReader(dr);
                    }
                }

                movie.ActorsInMovie = GetActors(movieID, cn);
                
            }

            return movie;
        }

        private List<Actor> GetActors(int movieID, SqlConnection cn)
        {
            var actorsInMovie = new List<Actor>();
            var cmd = new SqlCommand();

            cmd.CommandText = "GetActorsForMovie";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@movieid", movieID);
            cmd.Connection = cn;
            //cn.Open();

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    actorsInMovie.Add(ActorDataReader(dr));
                }
            }

            return actorsInMovie;
        }

        public int AddMovie(Movie newMovie)
        {
            int newIDNumber = 0;

            using (var cn = _connection)
            {
                var cmd = new SqlCommand();

                cmd.CommandText = "CreateNewMovieListing";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@title", newMovie.Title);
                cmd.Parameters.AddWithValue("@releasedate", newMovie.ReleaseDate);
                cmd.Parameters.AddWithValue("@mpaarating", newMovie.MPAARating);
                cmd.Parameters.AddWithValue("@director", newMovie.Director);
                cmd.Parameters.AddWithValue("@studio", newMovie.Studio);
                cmd.Parameters.AddWithValue("@userrating", newMovie.UserRating);
                cmd.Parameters.AddWithValue("@usernotes", newMovie.UserNotes);

                foreach (SqlParameter v in cmd.Parameters)
                {
                    if (v.Value == null)
                        v.Value = DBNull.Value;
                }

                cn.Open();
                newIDNumber = int.Parse(cmd.ExecuteScalar().ToString());

                if (newMovie.ActorsInMovie != null)
                {
                    AddAllActors(newMovie.ActorsInMovie, newIDNumber, cn);
                }
            }

            return newIDNumber;
        }

        private List<int> AddAllActors(List<Actor> actorList, int newIDNumber, SqlConnection cn)
        {
            var cmd = new SqlCommand();

            cmd.CommandText = "CheckForActors";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            List<int> movieActorIDList = new List<int>();

            foreach (var a in actorList)
            {
                int actorFound = 0;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@actorfirstname", a.ActorFirstName);
                cmd.Parameters.AddWithValue("@actorlastname", a.ActorLastName);

                actorFound = (int)cmd.ExecuteScalar();

                if (actorFound == 0)
                {
                    cmd.CommandText = "AddNewActor";

                    actorFound = (int)cmd.ExecuteScalar();
                }

                movieActorIDList.Add(CreateMovieActorListing(actorFound, newIDNumber, cn));
            }

            return movieActorIDList;
        }
        
        private int CreateMovieActorListing(int actorFound, int newIDnumber, SqlConnection cn)
        {
            var cmd = new SqlCommand();

            cmd.Connection = cn;
            cmd.CommandText = "AddActorMovieLink";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@movieid", newIDnumber);
            cmd.Parameters.AddWithValue("@actorid", actorFound);

            int newListingID = int.Parse(cmd.ExecuteScalar().ToString());

            return newListingID;
        }

        public int AddActorToMovie(int movieID, Actor a)
        {
            int actorFound = 0;
            using (var cn = _connection)
            {
                var cmd = new SqlCommand();
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@actorfirstname", a.ActorFirstName);
                cmd.Parameters.AddWithValue("@actorlastname", a.ActorLastName);
                cmd.Connection = cn;
                cmd.CommandText = "CheckForActors";
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                var checkFound = cmd.ExecuteScalar();
                if (checkFound != null)
                    actorFound = (int)checkFound;

                if (actorFound == 0)
                {
                    cmd.CommandText = "AddNewActor";
                    var something = int.Parse(cmd.ExecuteScalar().ToString());
                    actorFound = int.Parse(cmd.ExecuteScalar().ToString());
                }

                var newMovieActorID = CreateMovieActorListing(actorFound, movieID, cn);
            }

            return actorFound;
        }

        public void RemoveMovie(int movieID)
        {
            using (var cn = _connection)
            {
                cn.Open();

                RemoveMovieActorListings(movieID, cn);

                var cmd = new SqlCommand();

                cmd.CommandText = "RemoveMovie";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@movieid", movieID);
                cmd.Connection = cn;

                cmd.ExecuteNonQuery();
            }
        }

        private void RemoveMovieActorListings(int movieID, SqlConnection cn)
        {
            var actorList = GetActors(movieID, cn);

            if (actorList.Count != 0)
            {
                var cmd = new SqlCommand();

                cmd.CommandText = "RemoveMovieActorListings";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@movieid", movieID);

                //cn.Open();
                cmd.ExecuteNonQuery();

                foreach (var a in actorList)
                {
                    bool orphaned = IsOrphan(a.ActorID, cn);
                    if (orphaned)
                        RemoveOrphanActors(a.ActorID, cn);
                }
            }
        }

        private bool IsOrphan (int actorID, SqlConnection cn)
        {
            bool orphaned = false;

            var cmd = new SqlCommand();

            cmd.CommandText = "FoundActor";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@actorid", actorID);
            cmd.Connection = cn;

            int idReturned = 0;
            idReturned = (int)cmd.ExecuteScalar();

            if (idReturned == 0)
                orphaned = true;

            return orphaned;
        }

        private void RemoveOrphanActors(int actorID, SqlConnection cn)
        {
            bool isOrphaned = IsOrphan(actorID, cn);

            if (isOrphaned)
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "DeleteActor";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _connection;


            }
        }

        private Movie MovieDataReader(SqlDataReader dr)
        {
            Movie movie = new Movie();

            movie.MovieID = (int)dr["MovieID"];
            movie.Title = dr["Title"].ToString();

            if (dr["ReleaseDate"] != DBNull.Value)
                movie.ReleaseDate = (int)dr["ReleaseDate"];

            if (dr["MPAARating"] != DBNull.Value)
                movie.MPAARating = dr["MPAARating"].ToString();

            if (dr["Director"] != DBNull.Value)
                movie.Director = dr["Director"].ToString();

            if (dr["Studio"] != DBNull.Value)
                movie.Studio = dr["Studio"].ToString();

            if (dr["UserRating"] != DBNull.Value)
                movie.UserRating = (int)dr["UserRating"];

            if (dr["UserNotes"] != DBNull.Value)
                movie.UserNotes = dr["UserNotes"].ToString();

            return movie;
        }

        private Actor ActorDataReader(SqlDataReader dr)
        {
            Actor actor = new Actor();

            actor.ActorFirstName = dr["ActorFirstName"].ToString();
            actor.ActorLastName = dr["ActorLastName"].ToString();
            actor.ActorID = (int)dr["ActorID"];

            return actor;
        }
    }
}
