﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Actor
    {
        public int ActorID { get; set; }

        [Required(ErrorMessage ="Please enter the actor's first name.")]
        public string ActorFirstName { get; set; }

        [Required(ErrorMessage ="Please enter the actor's last name.")]
        public string ActorLastName { get; set; }
    }
}
