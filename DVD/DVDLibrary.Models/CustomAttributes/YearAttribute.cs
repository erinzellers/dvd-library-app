﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models.CustomAttributes
{
    public class YearAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool valid = false;
            if (value == null)
                valid = true;
            else
                valid = (int)value <= (int)DateTime.Now.Year;
            return valid;
        }
    }
}
