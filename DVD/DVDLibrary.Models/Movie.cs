﻿using DVDLibrary.Models.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Movie
    {
        public int MovieID { get; set; }

        [Required(ErrorMessage ="You must enter a title for the movie.")]
        public string Title { get; set; }

        [Year(ErrorMessage ="Movie release year cannot be in the future")]
        public int? ReleaseDate { get; set; }
        public string MPAARating { get; set; }
        public string Director { get; set; }
        public string Studio { get; set; }

        [Range(0,10)]
        public int? UserRating { get; set; }

        [DataType(DataType.MultilineText)]
        public string UserNotes { get; set; }

        public List<Actor> ActorsInMovie { get; set; }
    }
}
