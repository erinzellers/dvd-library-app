﻿using DVDLibrary.Controllers;
using DVDLibrary.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.ControllerTests
{
    [TestFixture]
    public class ControllerTests
    {
        [Test]
        public void DoesntReturnAMovie()
        {
            var controller = new MovieManagerController();
            var viewOut = controller.Details(200) as ViewResult;

            Assert.AreEqual("Error", viewOut.ViewName);
        }

        [Test]
        public void GetsMovieToDelete()
        {
            var controller = new MovieManagerController();
            var viewOut = controller.Delete(4) as ViewResult;
            Assert.AreEqual("Delete", viewOut.ViewName);
        }

        [Test]
        public void FindsMovieList()
        {
            var controller = new MovieManagerController();
            var viewOut = controller.ViewAll("Star Wars") as ViewResult;
            var moviesFound = (List<Movie>)viewOut.Model;
            Assert.AreEqual(3, moviesFound.Count);
        }
    }
}