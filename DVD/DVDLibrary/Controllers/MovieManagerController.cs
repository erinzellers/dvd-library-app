﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.Models;
using DVDLibrary.BLL;
using System.Net;
using DVDLibrary.Data;

namespace DVDLibrary.Controllers
{
    [HandleError]
    public class MovieManagerController : Controller
    {
        
        // GET: MovieManager
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int? id ,string searchString)
        {
            //SearchResult
            var manager = new Manager();
            Response<List<Movie>> response = manager.GetAllMovies(searchString);
            List<Movie> movieList = response.Data;
            TempData["results"] = movieList;
            return RedirectToAction("SearchResult");
        }
  
        public ActionResult SearchResult()
        {
            var res = TempData["results"];

            return View(res);
        }

        public ActionResult Details(int id)
        {
            var manager = new Manager();
            var response = manager.ViewSingleMovie(id);

            if (response.Success)
            {
                return View(response.Data);
            }
            else
                return View("Error");
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: MovieManager/Create
        [HttpPost]
        public ActionResult Create(Movie newMovie)
        {

            if (ModelState.IsValid)
            {

                Manager manager = new Manager();
                Response<Movie> response = manager.MovieAdd(newMovie);

                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
                return View("Error");
            }

            return View(newMovie);

        }

        
        // GET: MovieManager/Delete
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var manager = new Manager();
            var response = manager.ViewSingleMovie(id);

            if (response.Success)
            {
                var movieToDelete = response.Data;
                return View("Delete", movieToDelete);
            }
            else
            {
                return View("Error");
            }
        }

        // POST: MovieManager/Delete
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Manager manager = new Manager();
            Response<Movie> response = manager.MovieDelete(id);

            if (response.Success)
            {
                return RedirectToAction("Index");
            }

            return View("Error");
        }

        [HttpGet]
        public ActionResult ViewAll(string searchString)
        {
            var manager = new Manager();
            Response<List<Movie>> response = manager.GetAllMovies(searchString);
            List<Movie> movieList = response.Data;

            if (movieList == null)
            {
                return View("Error");
            }

            return View(movieList);
        }

        [HttpPost]
        public ActionResult AddActor(string FirstName, string LastName, int movieID)
        {
            Actor newActor = new Actor();
            newActor.ActorFirstName = FirstName;
            newActor.ActorLastName = LastName;

            var manager = new Manager();
            var response = manager.AddActorToMovie(newActor, movieID);

            if (response.Success)
                return RedirectToAction("Details", new { id = movieID });
            else
                return View("Error");
        }
        
    }
}
