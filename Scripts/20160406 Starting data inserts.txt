insert into Movie (Title, ReleaseDate, MPAARating, Director, Studio, UserRating, UserNotes)
values ('Star Wars', 1977, 'PG', 'George Lucas', '20th Century Fox', 5, 'Han shot first'),
('Star Wars: The Empire Strikes Back', 1980, 'PG', 'George Lucas', '20th Century Fox', 5, NULL),
('Star Wars: Return of the Jedi', 1983, 'PG', 'George Lucas', '20th Century Fox', 5, NULL),
('Raiders of the Lost Ark', 1981, 'PG', 'George Lucas', 'Paramount', 5, 'Snakes. Why''d it have to be snakes.'),
('The Night the Lights Went Off in Georgia', 1981, 'PG', 'Ron Maxwell', 'Viacom', 3, NULL),
('Jay and Silent Bob Strike Back', 2001, 'R', 'Kevin Smith', NULL, 4, NULL),
('Love Actually', 2003, 'R', 'Richard Curtis', 'Universal', 5, 'Bill Nighy is amazing'),
('Notting Hill', 1999, 'PG-13', 'Roger Mitchell', 'Working Title Films', 5, 'I''m a sucker for this movie.'),
('Flatliners', 1990, 'R', 'Joel Schumacher', 'Sony', 1, 'This movie was beyond bad.'),
('Bridget Jones''s Diary', 2001, 'R', 'Sharon Maguire', 'Miramax', 2, 'I just don''t see what all the fuss is about.'),
('Braveheart', 1995, 'R', 'Mel Gibson', 'Icon Entertainment', 4, 'Everyone loves Mel Gibson!')

insert into Actors (ActorFirstName, ActorLastName)
values ('Mark', 'Hamill'),
('Carrie', 'Fisher'),
('Harrison', 'Ford'),
('Kevin', 'Smith'),
('Jason', 'Mewes'),
('Hugh', 'Grant'),
('Julia', 'Roberts'),
('Renee', 'Zellweger'),
('Mel', 'Gibson')

insert into MovieActor (MovieID, ActorID)
values (1,1), (1,2), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3),
(4,3), (5,1), (6,2), (6,4), (6,5), (7,6), (8, 6), (8,7), (9,7), (10,8),
(10,6), (11,9)