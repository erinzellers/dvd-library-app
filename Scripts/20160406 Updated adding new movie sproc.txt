USE [AwesomeMovies]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewMovieListing]    Script Date: 4/6/2016 3:32:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[CreateNewMovieListing] (
@title varchar(50),
@releasedate int,
@mpaarating varchar(50),
@director varchar(50),
@studio varchar(50),
@userrating char(1),
@usernotes varchar(max)
) as

insert into Movie (Title, ReleaseDate, MPAARating, Director, Studio, UserRating, UserNotes)
values (@title, @releasedate, @mpaarating, @director, @studio, @userrating, @usernotes)

select SCOPE_IDENTITY();