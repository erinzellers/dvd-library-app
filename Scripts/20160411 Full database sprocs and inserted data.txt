USE [master]
GO

/****** Object:  Database [AwesomeMovies]    Script Date: 4/11/2016 12:45:45 PM ******/
CREATE DATABASE [AwesomeMovies]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Movies', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Movies.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Movies_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Movies_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [AwesomeMovies] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AwesomeMovies].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [AwesomeMovies] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [AwesomeMovies] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [AwesomeMovies] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [AwesomeMovies] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [AwesomeMovies] SET ARITHABORT OFF 
GO

ALTER DATABASE [AwesomeMovies] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [AwesomeMovies] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [AwesomeMovies] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [AwesomeMovies] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [AwesomeMovies] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [AwesomeMovies] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [AwesomeMovies] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [AwesomeMovies] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [AwesomeMovies] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [AwesomeMovies] SET  DISABLE_BROKER 
GO

ALTER DATABASE [AwesomeMovies] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [AwesomeMovies] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [AwesomeMovies] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [AwesomeMovies] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [AwesomeMovies] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [AwesomeMovies] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [AwesomeMovies] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [AwesomeMovies] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [AwesomeMovies] SET  MULTI_USER 
GO

ALTER DATABASE [AwesomeMovies] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [AwesomeMovies] SET DB_CHAINING OFF 
GO

ALTER DATABASE [AwesomeMovies] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [AwesomeMovies] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [AwesomeMovies] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [AwesomeMovies] SET  READ_WRITE 
GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[AddActorMovieLink]    Script Date: 4/11/2016 12:45:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[AddActorMovieLink] (
@actorid int,
@movieid int
) as

insert into MovieActor(ActorID, MovieID)
values (@actorid, @movieid)

select SCOPE_IDENTITY();

GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[AddNewActor]    Script Date: 4/11/2016 12:46:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[AddNewActor] (
@actorfirstname varchar(50),
@actorlastname varchar(50)
) as

insert into Actors (ActorFirstName, ActorLastName)
values (@actorfirstname, @actorlastname)

select SCOPE_IDENTITY();

GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[CheckForActors]    Script Date: 4/11/2016 12:47:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[CheckForActors] (
@actorfirstname varchar(50),
@actorlastname varchar(50)
) as

select ActorID, ActorFirstName, ActorLastName
from Actors
where ActorFirstName = @actorfirstname and ActorLastName = @actorlastname


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[CreateNewMovieListing]    Script Date: 4/11/2016 12:47:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[CreateNewMovieListing] (
@title varchar(50),
@releasedate int,
@mpaarating varchar(50),
@director varchar(50),
@studio varchar(50),
@userrating char(1),
@usernotes varchar(max)
) as

insert into Movie (Title, ReleaseDate, MPAARating, Director, Studio, UserRating, UserNotes)
values (@title, @releasedate, @mpaarating, @director, @studio, @userrating, @usernotes)

select SCOPE_IDENTITY();

GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[FoundActor]    Script Date: 4/11/2016 12:47:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[FoundActor] (
@actorid int
) as

select top(1) ActorID
from MovieActor
where ActorID = @actorid

select SCOPE_IDENTITY();
GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[GetActorsForMovie]    Script Date: 4/11/2016 12:47:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[GetActorsForMovie] (
@movieid int
) as

select m.Title, a.ActorFirstName, a.ActorLastName, a.ActorID
from MovieActor ma
join Actors a on a.ActorID = ma.ActorID
join Movie m on m.MovieID = ma.MovieID
where m.MovieID = @movieid


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[GetAllMovies]    Script Date: 4/11/2016 12:47:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[GetAllMovies] as

select *
from Movie


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[GetListByTitle]    Script Date: 4/11/2016 12:47:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[GetListByTitle] 
(
@movietitle varchar(50)
) as 

select *
from Movie
where Title like @movietitle
order by Title


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[GetMovieDetail]    Script Date: 4/11/2016 12:48:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[GetMovieDetail] (
@movieid int
) as

select *
from Movie
where MovieID = @movieid


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[RemoveMovie]    Script Date: 4/11/2016 12:48:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[RemoveMovie] (
@movieid int
) as

delete Movie where MovieID = @movieid


GO


USE [AwesomeMovies]
GO

/****** Object:  StoredProcedure [dbo].[RemoveMovieActorListings]    Script Date: 4/11/2016 12:48:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[RemoveMovieActorListings] (
@movieid int
) as

delete MovieActor
where MovieID = @movieid


GO


insert into Movie (Title, ReleaseDate, MPAARating, Director, Studio, UserRating, UserNotes)
values ('Star Wars', 1977, 'PG', 'George Lucas', '20th Century Fox', 5, 'Han shot first'),
('Star Wars: The Empire Strikes Back', 1980, 'PG', 'George Lucas', '20th Century Fox', 5, NULL),
('Star Wars: Return of the Jedi', 1983, 'PG', 'George Lucas', '20th Century Fox', 5, NULL),
('Raiders of the Lost Ark', 1981, 'PG', 'George Lucas', 'Paramount', 5, 'Snakes. Why''d it have to be snakes.'),
('The Night the Lights Went Off in Georgia', 1981, 'PG', 'Ron Maxwell', 'Viacom', 3, NULL),
('Jay and Silent Bob Strike Back', 2001, 'R', 'Kevin Smith', NULL, 4, NULL),
('Love Actually', 2003, 'R', 'Richard Curtis', 'Universal', 5, 'Bill Nighy is amazing'),
('Notting Hill', 1999, 'PG-13', 'Roger Mitchell', 'Working Title Films', 5, 'I''m a sucker for this movie.'),
('Flatliners', 1990, 'R', 'Joel Schumacher', 'Sony', 1, 'This movie was beyond bad.'),
('Bridget Jones''s Diary', 2001, 'R', 'Sharon Maguire', 'Miramax', 2, 'I just don''t see what all the fuss is about.'),
('Braveheart', 1995, 'R', 'Mel Gibson', 'Icon Entertainment', 4, 'Everyone loves Mel Gibson!')

insert into Actors (ActorFirstName, ActorLastName)
values ('Mark', 'Hamill'),
('Carrie', 'Fisher'),
('Harrison', 'Ford'),
('Kevin', 'Smith'),
('Jason', 'Mewes'),
('Hugh', 'Grant'),
('Julia', 'Roberts'),
('Renee', 'Zellweger'),
('Mel', 'Gibson')

insert into MovieActor (MovieID, ActorID)
values (1,1), (1,2), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3),
(4,3), (5,1), (6,2), (6,4), (6,5), (7,6), (8, 6), (8,7), (9,7), (10,8),
(10,6), (11,9)